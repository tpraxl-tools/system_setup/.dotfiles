export TERM="xterm-256color"

[ -f ~/.dotfiles/bash/oh-my-zsh ] && source ~/.dotfiles/bash/oh-my-zsh
[ -f ~/.dotfiles/home/.commonrc ] && source ~/.dotfiles/home/.commonrc

### asdf version manager (zsh)
if [ -f ~/.asdf/asdf.sh ]; then
  source ~/.asdf/asdf.sh
  #### completions (for bash, you would instead simply use the following line)
  # source ~/.asdf/completions/asdf.bash
  fpath=("${ASDF_DIR}/completions" $fpath)
  autoload -Uz compinit && compinit
fi

[ -f ~/.dotfiles/fzf/.fzf.zsh ] && source ~/.dotfiles/fzf/.fzf.zsh

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export DOTNET_CLI_TELEMETRY_OPTOUT=true

