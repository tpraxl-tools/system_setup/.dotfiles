= tpraxl's dotfiles

== Manual preparation

link:home/.gitconfig[.gitconfig] refers to an external file.
[source]
----
include::home/.gitconfig[tags=documentation.file-git-user]
----

You need to manually create that file and provide your personal data. E.g.:

[source]
----
[user]
	name = Eric Evans
	email = your@email.com
----

== Installation

[source,bash]
----
git clone https://gitlab.com/tpraxl-tools/system_setup/.dotfiles.git ~/.dotfiles
./install.bash "${HOME}"
----

== Generated Files

`.update-base` will contain a hint for future updating.
It is generated during installation and contains
an internal version-like id.
