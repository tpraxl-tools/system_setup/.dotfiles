#!/usr/bin/env bash

set -o errexit # exit when a command fails
set -o pipefail # return the exit status of the last command that threw a non-zero exit code
set -o nounset # exit when script tries to use undeclared variables

SCRIPT_PATH=$(realpath "${BASH_SOURCE[0]}")
# containing directory
SCRIPT_DIR=$(dirname "${SCRIPT_PATH}")

main() {
  validate_arguments "$@"
  install "$@"
}

validate_arguments() {
  if [ $# -ne 1 ]; then
    echo "Missing base-path." >&2
    echo "Usage: install.bash <base-path>".
    echo "Example: install.bash \"${HOME}\""
    exit 1
  fi
}

install() {
  local base="${1}"
  local relative_path_to_file
  local link
  for path in $(get_dotfiles "${SCRIPT_DIR}/home"); do
    relative_path_to_file=$(realpath --relative-to="${SCRIPT_DIR}/home" "${path}")
    link="${base}/${relative_path_to_file}"
    backup "${link}"
    provide_path_to "${link}"
    link "${path}" "${link}"
  done
  generate_update_base_file "${SCRIPT_DIR}" "0.1.0"
}

get_dotfiles() {
  local folder="${1}"
  find "${folder}" -type f
}

backup() {
  local file="${1}"
  if [[ -f "${file}" ]]; then
    mv "${file}" "${file}.bak"
  fi
}

provide_path_to() {
  local file="${1}"
  local parent_path
  parent_path="$(dirname "$file")"
  if [[ ! -d "${parent_path}" ]]; then
    mkdir -p "${parent_path}"
  fi
}

link() {
  local source_location="${1}"
  local symlink_location="${2}"
  if [[ -h "${symlink_location}" ]]; then
    echo "Did not link ${symlink_location}, because it already existed" >&2
  else
    ln -sv "${source_location}" "${symlink_location}"
  fi
}

generate_update_base_file() {
  local base="${1}"
  local internal_update_base_version="${2}"
  echo "${internal_update_base_version}" > "${base}/.update-base"
}

main "$@"
