#!/usr/bin/env bash

####
# Provides the convenience helpers `backup-encrypted` and `restore-encrypted`.
#
# Use them for adhoc folder backup & restore with gpg password encryption.
#
# If this script is sourced, it sets up the above mentioned aliases.
# If this script is used from `tar --use-compress-program`,
# then it detects that it is piped and executes the
# encryption.
#
# restore-encrypted can be used as a pipe target or with a (.tar.gz.gpg) file argument.
####

SCRIPT_PATH="${BASH_SOURCE:-$(realpath "${0}")}"

main() {
  if ! is_called_from_tar; then
    set_aliases
    return 0
  fi
  use_strict_mode
  compress
}

is_called_from_tar() {
  script_got_piped || return 1
}

script_got_piped() {
  [[ -p /dev/stdin ]] || return 1
}

set_aliases() {
  alias restore-encrypted='restore_encrypted'
  alias backup-encrypted='backup_encrypted'
  alias list-encrypted='list_encrypted'
}

restore_encrypted() {
  if script_got_piped; then
    gpg --decrypt | tar -xvzf -
  else
    gpg --decrypt "${1}" | tar -xvzf -
  fi
}

list_encrypted() {
  if script_got_piped; then
      gpg --decrypt | tar -ztvf -
  else
    gpg --decrypt "${1}" | tar -ztvf -
  fi
}

backup_encrypted() {
  local archive="${1:-}"
  local targets=( "${@:2}" )
  local this_script="${SCRIPT_PATH}"
  echo tar --use-compress-program="${this_script}" -cvf "${archive}" -C "$(pwd)" "${targets[@]}"
  tar --use-compress-program="${this_script}" -cvf "${archive}" -C "$(pwd)" "${targets[@]}"
}

use_strict_mode() {
  set -o errexit # exit when a command fails
  set -o pipefail # return the exit status of the last command that threw a non-zero exit code
  set -o nounset # exit when script tries to use undeclared variables
}

compress() {
  gzip -9 | gpg -c --cipher-algo AES256 --compress-algo none --yes -o - --trust-model=always -c
}

main "$@"
